package com.medstudioinc.rvfooterleaderadapter;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.medstudioinc.rvfooterleaderapdater.R;
import com.medstudioinc.rvfooterleaderlib.EndlessScrollListener;

import java.util.ArrayList;

/**
 * Author : Mohammmed Ajaroud
 * Birthday : 27/09/1992
 * Github : https://www.github.com/mohammedajaroud
 * Created At : 07/10/2018 - 13:15h PM
 * OS Version : Mac OS X Hight Sierra
 * Android Studio : 3.1.4
 * Target SDK : 28.0.0
 * Languages : Java/Kotlin
 */

public class JVMainActivity extends AppCompatActivity{
    RecyclerView rv;
    RvAdapter rvAdapter;
    ArrayList<Model> arrays;
    EndlessScrollListener endlessScrollListener;
    RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        arrays = new ArrayList<Model>();
        rv = (RecyclerView) findViewById(R.id.rv);
        layoutManager = new LinearLayoutManager(this);

        rv.setLayoutManager(layoutManager);

        arrays.add(new Model(0, "Item 0"));
        arrays.add(new Model(1, "Item 1"));
        arrays.add(new Model(2, "Item 2"));
        arrays.add(new Model(3, "Item 3"));
        arrays.add(new Model(4, "Item 4"));
        arrays.add(new Model(5, "Item 5"));
        arrays.add(new Model(6, "Item 6"));
        arrays.add(new Model(7, "Item 7"));
        arrays.add(new Model(8, "Item 8"));
        arrays.add(new Model(9, "Item 9"));

        rvAdapter = new RvAdapter(this);
        rvAdapter.setItems(arrays);
        rv.setAdapter(rvAdapter);

        endlessScrollListener = new EndlessScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, final int totalItemsCount, RecyclerView view) {
                rvAdapter.showLoading(true);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = totalItemsCount; i <= totalItemsCount+10; i++){
                            arrays.add(new Model(i, "Item " + i));
                            rvAdapter.notifyDataSetChanged();
                            rvAdapter.showLoading(false);
                        }
                    }
                }, 500);

            }
        };

        rv.addOnScrollListener(endlessScrollListener);


    }
}
