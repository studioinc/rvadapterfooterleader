package com.medstudioinc.rvfooterleaderadapter;

public class Model {
    public int id;
    public String item;

    public Model() {
    }

    public Model(int id, String item) {
        this.id = id;
        this.item = item;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

}
