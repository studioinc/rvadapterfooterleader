package com.medstudioinc.rvfooterleaderadapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.medstudioinc.rvfooterleaderapdater.R;
import com.medstudioinc.rvfooterleaderlib.FooterLoaderAdapter;

public class RvAdapter extends FooterLoaderAdapter<Model> {

    public RvAdapter(Context context) {
        super(context);
    }

    @Override
    public long getYourItemId(int position) {
        return mItems.get(position).getId();
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        return new MyHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false));
    }

    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
           MyHolder myHolder = (MyHolder) holder;
           Model model = mItems.get(position);
           myHolder.txtItem.setText(model.getItem());
           myHolder.itemView.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {

               }
           });
    }


    class MyHolder extends RecyclerView.ViewHolder{
        TextView txtItem;
        public MyHolder(@NonNull View itemView) {
            super(itemView);
            txtItem = (TextView) itemView.findViewById(R.id.txtItem);
        }
    }
}
