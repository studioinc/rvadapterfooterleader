package com.medstudioinc.rvfooterleaderadapter

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.medstudioinc.rvfooterleaderapdater.R
import com.medstudioinc.rvfooterleaderlib.EndlessScrollListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    internal lateinit var arrays: ArrayList<Model>
    internal lateinit var rvAdapter: RvAdapter
    internal lateinit var endlessScrollListener: EndlessScrollListener
     lateinit var layoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        arrays = arrayListOf()

        arrays.add(Model(0, "Item 0"));
        arrays.add(Model(1, "Item 1"));
        arrays.add(Model(2, "Item 2"));
        arrays.add(Model(3, "Item 3"));
        arrays.add(Model(4, "Item 4"));
        arrays.add(Model(5, "Item 5"));
        arrays.add(Model(6, "Item 6"));
        arrays.add(Model(7, "Item 7"));
        arrays.add(Model(8, "Item 8"));
        arrays.add(Model(9, "Item 9"));

        rvAdapter = RvAdapter(this)
        rvAdapter.setItems(arrays)

        layoutManager = LinearLayoutManager(this)
        rv.layoutManager = layoutManager
        rv.adapter = rvAdapter

        endlessScrollListener = object  : com.medstudioinc.rvfooterleaderlib.EndlessScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                rvAdapter.showLoading(true)
                Handler().postDelayed({
                    var i = totalItemsCount
                    while (i < totalItemsCount+10) {
                        arrays.add(Model(i, "Item $i"))
                        rvAdapter.notifyDataSetChanged()
                        rvAdapter.showLoading(false)
                        i ++
                    }
                }, 1000)
            }
        }

        rv.addOnScrollListener(endlessScrollListener)

    }
}
