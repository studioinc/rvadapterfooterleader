package com.medstudioinc.rvfooterleaderlib;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;


/**
 * Created by elanicdroid on 14/09/15.
 */
public class LoaderViewHolder extends RecyclerView.ViewHolder {
    ProgressBar mProgressBar;
    TextView textView;

    public LoaderViewHolder(View itemView) {
        super(itemView);
        mProgressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
        textView = (TextView) itemView.findViewById(R.id.text_view);
    }
}
