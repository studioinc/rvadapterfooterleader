package com.medstudioinc.rvfooterleaderlib;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;


import java.util.ArrayList;
import java.util.List;

public abstract class FooterLoaderAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    protected Context context;
    protected boolean showLoader;
    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;
    public static int lastAnimatedPosition = 0;

    protected List<T> mItems;
    protected LayoutInflater mInflater;

    public FooterLoaderAdapter(Context context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == VIEWTYPE_LOADER) {
            // Your Loader XML view here
            // Your LoaderViewHolder class
            return new LoaderViewHolder(mInflater.inflate(R.layout.loader_item_layout, viewGroup, false));
        } else if (viewType == VIEWTYPE_ITEM) {
            return getYourItemViewHolder(viewGroup);
        }
        throw new IllegalArgumentException("Invalid ViewType: " + viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        Animation animation = AnimationUtils.loadAnimation(context, (position > lastAnimatedPosition) ? R.anim.up_from_bottom : R.anim.up_from_bottom);
        viewHolder.itemView.startAnimation(animation);
        lastAnimatedPosition = position;
        // Loader ViewHolder
        if (viewHolder instanceof LoaderViewHolder) {
            LoaderViewHolder loaderViewHolder = (LoaderViewHolder)viewHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
                loaderViewHolder.textView.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
                loaderViewHolder.textView.setVisibility(View.GONE);
            }
            return;
        }
        bindYourViewHolder(viewHolder, position);

    }

    @Override
    public int getItemCount() {
        // If no items are present, there's no need for loader
        if (mItems == null || mItems.size() == 0) {
            return 0;
        }

        // +1 for loader
        return mItems.size();
    }


    @Override
    public long getItemId(int position) {

        // loader can't be at position 0
        // loader can only be at the last position
        if (position != 0 && position == getItemCount() - 1) {

            // id of loader is considered as -1 here
            return -1;
        }
        return getYourItemId(position);
    }

    @Override
    public int getItemViewType(int position) {

        // loader can't be at position 0
        // loader can only be at the last position
        if (position != 0 && position == getItemCount() - 1) {
            return this.VIEWTYPE_LOADER;
        }

        return this.VIEWTYPE_ITEM;
    }

    public void showLoading(boolean status) {
        this.showLoader = status;
    }

    public void setItems(ArrayList<T> items) {
        this.mItems = items;
    }

    public void addItems(List<T> items) {
        this.mItems.addAll(items);
        notifyDataSetChanged();
    }

    public abstract long getYourItemId(int position);
    public abstract RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent);
    public abstract void bindYourViewHolder(RecyclerView.ViewHolder holder, int position);

}
